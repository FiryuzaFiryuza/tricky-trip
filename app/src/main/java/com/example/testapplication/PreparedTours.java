package com.example.testapplication;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.example.testapplication.adapter.MenuListAdapter;
import com.example.testapplication.appDataBase.TourDataBaseHelper;
import com.example.testapplication.model.entities.ReadyTourEntity;

import java.io.IOException;
import java.util.ArrayList;


public class PreparedTours extends Activity {

    public static boolean isSystemTour;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_prepared_tours);
        setTitle("Готовый маршрут");

        TourDataBaseHelper dataBaseHelper = null;
        try {
            dataBaseHelper = new TourDataBaseHelper(this);
        } catch (IOException e) {
            e.printStackTrace();
        }

        final ArrayList<ReadyTourEntity> list = dataBaseHelper.getAllReadyTours();
        dataBaseHelper.close();
        MenuListAdapter adapter = new MenuListAdapter(list, this);
        final ListView listView = (ListView) findViewById(R.id.lv_ready_packages);
        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                TourData.ReadyTour.setSystemTour(list.get(position).isSystemTour());
                isSystemTour = list.get(position).isSystemTour();

                Intent intent = new Intent(PreparedTours.this, TourData.class);
                intent.putExtra("item", list.get(position).getName());
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

                startActivity(intent);
            }
        });
    /*    listView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                    View.DragShadowBuilder shadowBuilder = new View.DragShadowBuilder(view);
                    view.startDrag(null, shadowBuilder, view, 0);
                    return true;
                } else {
                    return false;
                }
            }
        });
        listView.setOnDragListener(new View.OnDragListener() {
            @Override
            public boolean onDrag(View view, DragEvent dragEvent) {
                int action = dragEvent.getAction();
                switch (action) {
                    case DragEvent.ACTION_DROP :
                      list.remove(1);
                      view.refreshDrawableState();
                      break;
                }
                return true;
            }
        });*/
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
