package com.example.testapplication;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.testapplication.appDataBase.TourDataBaseHelper;
import com.example.testapplication.model.entities.SightsEntity;

import java.io.IOException;


public class SightsInfo extends Activity {

    private String title;
    private TextView address;
    private TextView workingTime;
    private TextView dinnerTime;
    private TextView description;
    private ImageView imagePath;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sights_info);

        TourDataBaseHelper db = null;
        try {
            db = new TourDataBaseHelper(this);
        } catch (IOException e) {
            e.printStackTrace();
        }

        Bundle name = getIntent().getExtras();
        this.title = name.getString("sightsInfo");
        SightsEntity sightsInfoModel = db.getSightsInfo(this.title);

        db.close();

        this.address = (TextView) findViewById(R.id.tv_address);
        this.address.setText(sightsInfoModel.getAddress());

        //this.workingTime = (TextView) findViewById(R.id.tv_working_time);
        //this.workingTime.setText(sightsInfoModel.getFrom().toString() + "-" + sightsInfoModel.getTo().toString());

        //this.dinnerTime = (TextView) findViewById(R.id.tv_dinner_time);
        //this.dinnerTime.setText(sightsInfoModel.getDinnerTimeFrom().toString() + "-" + sightsInfoModel.getDinnerTimeTo().toString());

        this.description = (TextView) findViewById(R.id.tv_description);
        this.description.setText(sightsInfoModel.getDescription());

        //this.imagePath = (ImageView) findViewById(R.id.imgv_sights_photo);
        //this.imagePath.setImageDrawable(sightsInfoModel.getImagePath());
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.sights__info, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
