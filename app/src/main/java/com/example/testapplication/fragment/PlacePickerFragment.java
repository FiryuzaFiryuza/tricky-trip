package com.example.testapplication.fragment;

import android.app.Activity;
import android.net.Uri;
import android.os.Bundle;
import android.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.example.testapplication.R;
import com.example.testapplication.adapter.IconListViewAdapter;
import com.example.testapplication.adapter.SightsListWithInfo;
import com.example.testapplication.appDataBase.TourDataBaseHelper;
import com.example.testapplication.model.entities.SightsEntity;

import java.io.IOException;
import java.util.ArrayList;


public class PlacePickerFragment extends Fragment implements  TextView.OnEditorActionListener{

    private ListView placesGroupsList;
    private ListView placesListView;
    private EditText newTourName;
    private String tourName;

    public PlacePickerFragment(String name) {
        this.tourName = name;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_place_picker, null, false);

        TourDataBaseHelper dataBaseHelper = null;
        try {
            dataBaseHelper = new TourDataBaseHelper(v.getContext());
        } catch (IOException e) {
            e.printStackTrace();
        }

        ArrayList<Integer> icons=new ArrayList<Integer>();
        icons.add(R.drawable.cultural_legacy);
        icons.add(R.drawable.presentations);
        icons.add(R.drawable.cinemas);
        icons.add(R.drawable.sport_objects);

        placesGroupsList = (ListView)v.findViewById(R.id.placesGroupsView);
        placesGroupsList.setAdapter(new IconListViewAdapter(v.getContext(), R.layout.icon_list_item,icons));

        final SightsListWithInfo[] adapters = new SightsListWithInfo[icons.size()];
        ArrayList<ArrayList<SightsEntity>> places = new ArrayList<ArrayList<SightsEntity>>();

        for(int i = 1; i <= 2; i++) {
            ArrayList<SightsEntity> temp =  dataBaseHelper.getSightsFromCategoryById(i);
            places.add(temp);
            adapters[i - 1] = new SightsListWithInfo(temp, v.getContext());
        }

        placesListView = (ListView)v.findViewById(R.id.placesListView);

        placesGroupsList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                placesListView.setAdapter(adapters[position]);
            }
        });

        this.newTourName = (EditText) v.findViewById(R.id.editText_name);
        this.newTourName.setText(this.tourName);
        this.newTourName.setOnEditorActionListener(this);
        /*this.newTourName.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable s) {
                newTourName.setText(newTourName.getText());
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }
        });*/

        return v;
    }


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
        return false;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onFragmentInteraction(Uri uri);
    }

    public String getTextFromEdit() {
        return this.newTourName.getText().toString();
    }

    public void setTextFromEdit(String name) {

        this.newTourName.setText(name);
    }
}
