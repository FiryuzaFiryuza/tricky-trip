package com.example.testapplication.fragment;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.testapplication.R;
import com.example.testapplication.TourData;
import com.example.testapplication.adapter.SightsListAdapter;
import com.example.testapplication.appDataBase.TourDataBaseHelper;
import com.example.testapplication.model.entities.SightsEntity;

import java.io.IOException;
import java.util.ArrayList;

public class DataTourList extends Fragment implements View.OnClickListener, TextView.OnEditorActionListener {

    private Context context;
    private int index;
    private String name;
    private SightsListAdapter adapter;
    private static EditText tourName;

    private Button showBttn;

    public DataTourList() { }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_data_tour_list, null, false);

        TourDataBaseHelper db = null;
        try {
            db = new TourDataBaseHelper(v.getContext());
        } catch (IOException e) {
            e.printStackTrace();
        }

        this.index = db.getReadyTourIdByName(this.name);
        ArrayList<SightsEntity> list = db.getAllSightsFromTour(this.index);

        TourData.AddedItems.addAll(list);
        TourData.ReadyTour = db.getReadyTour(this.index);

        db.close();

        this.showBttn = (Button) v.findViewById(R.id.btn_show);
        this.showBttn.setOnClickListener(this);

        final TextView tv = (TextView) v.findViewById(R.id.textView);
        this.tourName = (EditText) v.findViewById(R.id.editText_name);
        this.tourName.setText(TourData.ReadyTour.getName());
        //this.tourName.setOnEditorActionListener(this);
 /*       this.tourName.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable s) {
                tourName.setText(tourName.getText());
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                tourName.setText(tourName.getText());
            }
        });*/

        adapter = new SightsListAdapter(list, v.getContext());

        ListView listView = (ListView)v.findViewById(R.id.lv_sights);
        listView.setAdapter(adapter);

        return v;
    }

    public SightsListAdapter getAdapter() {
        return this.adapter;
    }

    public void setName(String name) {
        this.name = name;
    }

    public static String getTextFromEdit() {
        //Log.d("tourname",this.tourName.getText().toString());
        String name = tourName.getText().toString();
        return name;

    }

    @Override
    public void onClick(View view) {
        Log.d("show", getTextFromEdit());
    }

    @Override
    public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
        return false;
    }
}
