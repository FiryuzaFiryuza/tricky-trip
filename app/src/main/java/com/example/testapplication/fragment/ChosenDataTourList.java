package com.example.testapplication.fragment;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ListView;

import com.example.testapplication.PlacePicker;
import com.example.testapplication.R;
import com.example.testapplication.TourData;
import com.example.testapplication.adapter.SightsListAdapter;
import com.example.testapplication.model.entities.SightsEntity;

import java.util.List;

public class ChosenDataTourList extends Fragment implements View.OnClickListener {

    private Context context;
    private String name;
    private SightsListAdapter adapter;
    private EditText tourName;

    public ChosenDataTourList() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_chosen_data_tour_list, container, false);

        this.tourName = (EditText) v.findViewById(R.id.editText_name);
        this.tourName.setText(name);

        if(PlacePicker.IsFromPreparedTour) {

            this.filterList(TourData.AddedItems, TourData.DeletedItems);
            adapter = new SightsListAdapter(TourData.AddedItems, v.getContext());
        }
        else {

            this.filterList(PlacePicker.NewAddedItems, PlacePicker.NewDeletedItems);
            adapter = new SightsListAdapter(PlacePicker.NewAddedItems, v.getContext());
        }


        ListView listView = (ListView)v.findViewById(R.id.lv_sights);
        listView.setAdapter(adapter);

        return v;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTextFromEdit() {
        return this.tourName.getText().toString();
    }

    @Override
    public void onClick(View view) {

    }

    private void filterList(List<SightsEntity> addedList, List<SightsEntity> deletedList) {
        for(int i = 0; i < deletedList.size(); i++) {
            if(addedList.contains(deletedList.get(i))) {
                addedList.remove(deletedList.get(i));
            }
        }
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onFragmentInteraction(Uri uri);
    }

}
