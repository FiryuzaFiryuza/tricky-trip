package com.example.testapplication.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;

import com.example.testapplication.R;

import java.util.List;

/**
 * Created by Надир on 02.12.2014.
 */
public class IconListViewAdapter extends ArrayAdapter<Integer> {
    private Context context;
    private int layoutResourceId;
    private List<Integer> list;

    public IconListViewAdapter(Context context, int layoutResourceId, List<Integer> list) {
        super(context, layoutResourceId, list);
        this.layoutResourceId = layoutResourceId;
        this.context = context;
        this.list = list;
    }

    static class ViewHolder{
        int item;
        protected ImageView itemIcon;
    }

    public View getView(final int position, View convertView, ViewGroup parent)
    {

        final ArrayAdapter<Integer> temp = this;
        View row = convertView;
        ViewHolder holder = null;
        LayoutInflater inflater = ((Activity)context).getLayoutInflater();
        row = inflater.inflate(layoutResourceId, parent, false);
        holder = new ViewHolder();
        holder.item = list.get(position);
        holder.itemIcon = (ImageView)row.findViewById(R.id.icon);
        holder.itemIcon.setImageResource(list.get(position));
        row.setTag(holder);
        return row;
    }

}