package com.example.testapplication.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.View;

import com.example.testapplication.AboutProgram;
import com.example.testapplication.MainMenu;
import com.example.testapplication.PlacePicker;
import com.example.testapplication.PreparedTours;
import com.example.testapplication.R;

import java.util.ArrayList;

/**
 * Created by Надир on 29.11.2014.
 */
public class onMenuButtonClickListener implements View.OnClickListener {
    Context context;
    public onMenuButtonClickListener(Context context){
        super();
        this.context = context;
    }
    public void onClick(View view) {
        switch(view.getId()) {
            case R.id.preparedPathButton:
                Intent intent = new Intent(context, PreparedTours.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);
                break;
            case R.id.createPathButton:
                intent = new Intent(context, PlacePicker.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);
                break;
            case R.id.currentPathButton:

                break;
            case R.id.aboutProgramButton:
                intent = new Intent(context, AboutProgram.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);
            break;
        }
    }
}
