package com.example.testapplication.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;

import com.example.testapplication.PlacePicker;
import com.example.testapplication.R;
import com.example.testapplication.SightsInfo;
import com.example.testapplication.TourData;
import com.example.testapplication.model.entities.SightsEntity;

import java.util.ArrayList;

/**
 * Created by Фирюза on 04.11.2014.
 */
public class SightsListAdapter extends BaseAdapter {

    private ArrayList<SightsEntity> sightsList;
    private Context context;

    private class ViewHolder {
        TextView tvName;
        Button bttnInfo;
        Button bttnDelete;
    }

    private ViewHolder viewHolder;

    public SightsListAdapter(ArrayList<SightsEntity> sightsList, Context context) {
        this.sightsList = sightsList;
        this.context = context;
    }

    @Override
    public int getCount() {
        return this.sightsList.size();
    }

    @Override
    public Object getItem(int i) {
        return this.sightsList.get(i);
    }

    public ArrayList<SightsEntity> getSightsList() {
        return this.sightsList;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup viewGroup) {
        final SightsListAdapter temp = this;
        if(convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.activity_item_from_ready_tour_list, null);

            viewHolder = new ViewHolder();
            viewHolder.tvName = (TextView) convertView.findViewById(R.id.tv_sight_name);
            viewHolder.bttnInfo = (Button) convertView.findViewById(R.id.bttn_sight_info);
            viewHolder.bttnDelete = (Button) convertView.findViewById(R.id.bttn_sight_delete);
            convertView.setTag(viewHolder);
        }
        else
        {
            viewHolder = (ViewHolder)convertView.getTag();
        }

        viewHolder.bttnInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int id = position;
                String name = sightsList.get(id).getName();

                Intent intent = new Intent(context, SightsInfo.class);
                intent.putExtra("sightsInfo", name);
                context.startActivity(intent);
            }
        });

        viewHolder.bttnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int item = position;

                if(PlacePicker.IsFromPreparedTour) {
                    TourData.DeletedItems.add(sightsList.get(item));
                }
                else {
                    PlacePicker.NewDeletedItems.add(sightsList.get(item));
                }

                boolean find = false;
                for (int i = 0; i < sightsList.size() && !find; i++) {
                    if (sightsList.get(i).getName().equals(sightsList.get(item).getName())) {
                        sightsList.remove(i);
                        find = true;
                        temp.notifyDataSetChanged();
                    }
                }
            }
        });

        viewHolder.tvName.setText(this.sightsList.get(position).getName());

        convertView.setTag(viewHolder);

        return convertView;
    }
}
