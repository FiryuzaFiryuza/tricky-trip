package com.example.testapplication.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;

import com.example.testapplication.PlacePicker;
import com.example.testapplication.R;
import com.example.testapplication.SightsInfo;
import com.example.testapplication.TourData;
import com.example.testapplication.model.entities.SightsEntity;

import java.util.ArrayList;

/**
 * Created by Надир on 01.12.2014.
 */
public class SightsListWithInfo extends BaseAdapter {

    private ArrayList<SightsEntity> sightsList;
    private boolean[] picked;
    private Context context;

    private class ViewHolder {
        TextView tvName;
        Button bttnInfo;
    }

    private ViewHolder viewHolder;

    public SightsListWithInfo(ArrayList<SightsEntity> sightsList, Context context) {
        this.sightsList = sightsList;
        this.context = context;
        picked = new boolean[sightsList.size()];
    }

    @Override
    public int getCount() {
        return this.sightsList.size();
    }

    @Override
    public Object getItem(int i) {
        return this.sightsList.get(i);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup viewGroup) {

        final BaseAdapter temp = this;
        if(convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.sights_item_with_info, null);

            viewHolder = new ViewHolder();
            viewHolder.tvName = (TextView) convertView.findViewById(R.id.tv_sight_name);
            viewHolder.bttnInfo = (Button) convertView.findViewById(R.id.bttn_sight_info);
            convertView.setTag(viewHolder);
        }
        else
        {
            viewHolder = (ViewHolder)convertView.getTag();
        }

        viewHolder.bttnInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int id = position;
                String name = sightsList.get(id).getName();

                Intent intent = new Intent(context, SightsInfo.class);
                intent.putExtra("sightsInfo", name);
                context.startActivity(intent);
            }
        });

        convertView.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {
                picked[position] = !picked[position];
                if (picked[position]){
                    v.setBackgroundResource(R.drawable.place_text_view_green);
                    viewHolder.tvName.setTextColor(0xFFFFFFFF);

                    if(PlacePicker.IsFromPreparedTour) {

                        TourData.AddedItems.add(sightsList.get(position));
                    }
                    else {
                        PlacePicker.NewAddedItems.add(sightsList.get(position));
                    }

                }
                else {
                    v.setBackgroundResource(R.drawable.place_text_view);
                    viewHolder.tvName.setTextColor(0xFF186306);

                    if(PlacePicker.IsFromPreparedTour) {

                        TourData.DeletedItems.add(sightsList.get(position));
                    }
                    else {
                        PlacePicker.NewDeletedItems.add(sightsList.get(position));
                    }
                }

            }
        });

        viewHolder.tvName.setText(this.sightsList.get(position).getName());
        if (picked[position]){
            convertView.setBackgroundResource(R.drawable.place_text_view_green);
            viewHolder.tvName.setTextColor(0xFFFFFFFF);
        }
        else {
            convertView.setBackgroundResource(R.drawable.place_text_view);
            viewHolder.tvName.setTextColor(0xFF186306);
        }

        convertView.setTag(viewHolder);

        return convertView;
    }

    public ArrayList<SightsEntity> getPickedPlacesArray()
    {
        ArrayList<SightsEntity> pickedSightsList= new ArrayList<SightsEntity>();
        for (int i=0; i<sightsList.size(); i++)
            if (picked[i])
                pickedSightsList.add(sightsList.get(i));
        return pickedSightsList;
    }
}