package com.example.testapplication.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.testapplication.R;
import com.example.testapplication.model.entities.ReadyTourEntity;

import java.util.ArrayList;

/**
 * Created by Фирюза on 04.11.2014.
 */
public class MenuListAdapter extends BaseAdapter {

    private ArrayList<ReadyTourEntity> menuList;
    private Context context;

    private class ViewHolder {
        TextView tvTourName;
    }

    private ViewHolder viewHolder;

    public MenuListAdapter(ArrayList<ReadyTourEntity> list, Context context) {
        this.menuList = list;
        this.context = context;
    }

    @Override
    public int getCount() {
        return this.menuList.size();
    }

    @Override
    public Object getItem(int i) {
        return this.menuList.get(i);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup viewGroup) {
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.activity_tour_name, null);

            viewHolder = new ViewHolder();
            viewHolder.tvTourName = (TextView) convertView.findViewById(R.id.tv_tour_name);

            convertView.setTag(viewHolder);
        }
        else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        viewHolder.tvTourName.setText(this.menuList.get(position).getName());

        return convertView;
    }
}
