package com.example.testapplication.model.entities;

/**
 * Created by Фирюза on 17.11.2014.
 */
public class SightsFromTourEntity extends BaseEntity  {
    private ReadyTourEntity readyTour;
    private SightsEntity sightsEntity;

    public  SightsFromTourEntity() {
        super();
    }

    public SightsFromTourEntity(int id, ReadyTourEntity readyTour, SightsEntity sightsEntity) {
        super(id);
        this.readyTour = readyTour;
        this.sightsEntity = sightsEntity;
    }

    public ReadyTourEntity getReadyTour() {
        return readyTour;
    }

    public void setReadyTour(ReadyTourEntity readyTour) {
        this.readyTour = readyTour;
    }

    public SightsEntity getSightsEntity() {
        return sightsEntity;
    }

    public void setSightsEntity(SightsEntity sightsEntity) {
        this.sightsEntity = sightsEntity;
    }
}
