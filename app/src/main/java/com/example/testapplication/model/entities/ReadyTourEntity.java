package com.example.testapplication.model.entities;

/**
 * Created by Фирюза on 17.11.2014.
 */
public class ReadyTourEntity extends BaseEntity {
    private String name;
    private boolean isSystemTour;

    public ReadyTourEntity() {
        super();
    }
    public ReadyTourEntity(int id, String name, boolean isSystemTour) {
        super(id);
        this.name = name;
        this.isSystemTour = isSystemTour;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isSystemTour() {
        return isSystemTour;
    }

    public void setSystemTour(boolean isSystemTour) {
        this.isSystemTour = isSystemTour;
    }
}
