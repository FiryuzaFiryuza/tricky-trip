package com.example.testapplication.model.entities;

/**
 * Created by Фирюза on 17.11.2014.
 */
public class CurrentOptimumPlan extends BaseEntity {

    private SightsEntity sightsEntityId;
    private int sequenceIndex;

    public CurrentOptimumPlan(int id, SightsEntity sightsEntityId) {
        super(id);
        this.sightsEntityId = sightsEntityId;
    }

    public int getSequenceIndex() {
        return sequenceIndex;
    }

    public void setSequenceIndex(int sequenceIndex) {
        this.sequenceIndex = sequenceIndex;
    }

    public int getSightsEntityId() {
        return sightsEntityId.getId();
    }

    public void setSightsEntityId(SightsEntity sightsEntityId) {
        this.sightsEntityId = sightsEntityId;
    }
}
