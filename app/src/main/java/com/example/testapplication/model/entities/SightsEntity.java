package com.example.testapplication.model.entities;

import android.text.format.Time;

/**
 * Created by Фирюза on 17.11.2014.
 */
public class SightsEntity  extends BaseEntity {
    private String name;
    private String address;
    private Time from;
    private Time to;
    private String description;
    private int image;
    private Time dinnerTimeFrom;
    private Time dinnerTimeTo;
    private int categoriesId;
    private CategoriesEntity category;

    public SightsEntity() {
        super();
    }

    public SightsEntity(int id, String name, String address, Time from, Time to, Time dinnerTimeFrom, Time dinnerTimeTo) {
        super(id);
        this.name = name;
        this.address = address;
        this.from = from;
        this.to = to;
        this.dinnerTimeFrom = dinnerTimeFrom;
        this.dinnerTimeTo = dinnerTimeTo;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Time getFrom() {
        return from;
    }

    public void setFrom(Time from) {
        this.from = from;
    }

    public Time getTo() {
        return to;
    }

    public void setTo(Time to) {
        this.to = to;
    }

    public Time getDinnerTimeFrom() {
        return dinnerTimeFrom;
    }

    public void setDinnerTimeFrom(Time dinnerTimeFrom) {
        this.dinnerTimeFrom = dinnerTimeFrom;
    }

    public Time getDinnerTimeTo() {
        return dinnerTimeTo;
    }

    public void setDinnerTimeTo(Time dinnerTimeTo) {
        this.dinnerTimeTo = dinnerTimeTo;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }

    public int getCategoriesId() {
        return categoriesId;
    }

    public void setCategoriesId(int categoriesId) {
        this.categoriesId = categoriesId;
    }

    public CategoriesEntity getCategory() {
        return category;
    }

    public void setCategory(CategoriesEntity category) {
        this.category = category;
    }
}
