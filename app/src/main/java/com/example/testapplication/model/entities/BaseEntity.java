package com.example.testapplication.model.entities;

/**
 * Created by Фирюза on 17.11.2014.
 */
public class BaseEntity {
    protected int id;

    public BaseEntity() {}

    public BaseEntity(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
