package com.example.testapplication.model.entities;

/**
 * Created by Фирюза on 09.12.2014.
 */
public class CategoriesEntity extends BaseEntity {
    private String name;

    public CategoriesEntity() {}

    public CategoriesEntity(String name) {
        this.name = name;
    }

    public CategoriesEntity(int id, String name) {
        super(id);
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
