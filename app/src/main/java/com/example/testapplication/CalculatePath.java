package com.example.testapplication;

import android.app.Activity;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TimePicker;


public class CalculatePath extends Activity {
    Button timeFrom;
    Button timeTo;
    Button calculatePathButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calculate_path);
        ArrayAdapter<String> transportAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item,
                new String[]{"Пешком","Машина","Такси","Автобус"});
        transportAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        Spinner spinner = (Spinner)this.findViewById(R.id.transportPicker);

        spinner.setAdapter(transportAdapter);
        spinner.setSelection(0);

        timeFrom = (Button)this.findViewById(R.id.timeFrom);
        timeTo = (Button) this.findViewById(R.id.timeTo);

        timeFrom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TimePickerDialog.OnTimeSetListener myCallBack = new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int hour, int minute) {
                        String result=String.valueOf(hour);
                        if (hour<10) result='0'+result;
                        result+=':';
                        if (minute<10) result+='0';
                        result+=minute;
                        timeFrom.setText(result);
                    }
                };
                TimePickerDialog chooseTime = new TimePickerDialog(CalculatePath.this,myCallBack,0,0,true);
                chooseTime.show();
            }
        });

        timeTo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TimePickerDialog.OnTimeSetListener myCallBack = new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int hour, int minute) {
                        String result=String.valueOf(hour);
                        if (hour<10) result='0'+result;
                        result+=':';
                        if (minute<10) result+='0';
                        result+=minute;
                        timeTo.setText(result);
                    }
                };
                TimePickerDialog chooseTime = new TimePickerDialog(CalculatePath.this,myCallBack,0,0,true);
                chooseTime.show();
            }
        });

        calculatePathButton =(Button)findViewById(R.id.calculatePathButton);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}