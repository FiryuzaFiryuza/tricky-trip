package com.example.testapplication;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;

import com.example.testapplication.adapter.onMenuButtonClickListener;

import java.util.ArrayList;


public class MainMenu extends Activity {
    onMenuButtonClickListener buttonClickListener;
    private static ArrayList<String> places;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_menu);

        buttonClickListener = new onMenuButtonClickListener(MainMenu.this);

        Button preparedPathButton = (Button) findViewById(R.id.preparedPathButton);
        Button createPathButton = (Button) findViewById(R.id.createPathButton);
        Button currentPathButton = (Button) findViewById(R.id.currentPathButton);
        Button aboutProgramButton = (Button) findViewById(R.id.aboutProgramButton);

        preparedPathButton.setOnClickListener(buttonClickListener);
        createPathButton.setOnClickListener(buttonClickListener);
        currentPathButton.setOnClickListener(buttonClickListener);
        aboutProgramButton.setOnClickListener(buttonClickListener);
        places = new ArrayList<String>();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public static ArrayList<String> getPlaces()
    {
        return places;
    }

    public static void setPlaces(ArrayList<String> pl)
    {
        places = pl;
    }


}
