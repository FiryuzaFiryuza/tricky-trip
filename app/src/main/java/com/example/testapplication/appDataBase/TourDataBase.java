package com.example.testapplication.appDataBase;

import android.content.Context;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * Created by Фирюза on 22.11.2014.
 */
public class TourDataBase extends SQLiteOpenHelper {

    private static String DATA_BASE_PATH = "data//data//com.example.testapplication//databases//";

    private SQLiteDatabase myDataBase;

    private final Context context;

    public static final String DATA_BASE_NAME = "trickytrip.sqlite";
    public static final int DATA_BASE_VERSION = 1;

    // tables in db
    public static final String TABLE_SIGHTS = "sights";
    public static final String TABLE_READY_TOUR = "readytour";
    public static final String TABLE_SIGHTS_FROM_TOUR = "sightsfromtour";
    public static final String TABLE_CURRENT_OPTIMUM_PLAN = "currentoptimumplan";
    public static final String TABLE_CATEGORIES = "categories";

    // common fileds in tables
    public static final String KEY_ID = "_id";
    public static final String KEY_NAME = "name";

    // other fileds
    // TABLE_SIGHTS
    public static final String KEY_ADDRESS = "address";
    public static final String KEY_WORKING_TIME_FROM = "from";
    public static final String KEY_WORKING_TIME_TO = "to";
    public static final String KEY_DESCRIPTION = "description";
    public static final String KEY_IMAGE = "image";
    public static final String KEY_DINNER_TIME_FROM = "dinnertimefrom";
    public static final String KEY_DINNER_TIME_TO = "dinnertimeto";
    public static final String KEY_CATEGORIES_ID = "categories_id";

    // TABLE_READY_TOUR
    public static final String KEY_IS_SYSTEM_TOUR = "issystemtour";

    // TABLE_SIGHTS_FROM_TOUR
    public static final String KEY_TOUR_ID = "tour_id";
    // TABLE_CURRENT_OPTIMUM_PLAN and TABLE_SIGHTS_FROM_TOUR
    public static final String KEY_SIGHTS_ID = "sight_id";
    // TABLE_CURRENT_OPTIMUM
    public static final String KEY_SEQUENCE_INDEX = "sequenceindex";

    public TourDataBase(Context context) {
        super(context, DATA_BASE_NAME, null, DATA_BASE_VERSION);
        this.context = context;
    }

    /**
     * Creates a empty database on the system and rewrites it with your own database.
     * */
    public void createDataBase() throws IOException {

        boolean dbExist = checkDataBase();

        this.getWritableDatabase();

        try {
            copyDataBase();
        }
        catch (IOException e) {
            throw new Error("Error copying database");
        }
/*
        if(dbExist){
            //do nothing - database already exist
        }
        else{
            //By calling this method and empty database will be created into the default system path
            //of your application so we are gonna be able to overwrite that database with our database.
            this.getWritableDatabase();

            try {
                copyDataBase();
            }
            catch (IOException e) {
                throw new Error("Error copying database");
            }
        }
*/
        this.openDataBase();
    }

    public void openDataBase() throws SQLException {
        //Open the database
        String myPath = DATA_BASE_PATH + DATA_BASE_NAME;
        myDataBase = SQLiteDatabase.openDatabase(myPath, null, SQLiteDatabase.OPEN_READWRITE);
    }

    @Override
    public synchronized void close() {
        if(myDataBase != null)
            myDataBase.close();
        super.close();
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {

    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i2) {
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + TABLE_SIGHTS + ";");
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + TABLE_READY_TOUR + ";");
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + TABLE_SIGHTS_FROM_TOUR + ";");
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + TABLE_CURRENT_OPTIMUM_PLAN + ";");

        onCreate(sqLiteDatabase);
    }

    /**
     * Check if the database already exist to avoid re-copying the file each time you open the application.
     * @return true if it exists, false if it doesn't
     */
    private boolean checkDataBase(){

        SQLiteDatabase checkDB = null;

        try {
            String myPath = DATA_BASE_PATH + DATA_BASE_NAME;
            checkDB = SQLiteDatabase.openDatabase(myPath, null, SQLiteDatabase.OPEN_READWRITE);

        }
        catch(SQLiteException e){
            //database does't exist yet.
        }

        if(checkDB != null){
            checkDB.close();
        }

        return checkDB != null ? true : false;
    }

    /**
     * Copies your database from your local assets-folder to the just created empty database in the
     * system folder, from where it can be accessed and handled.
     * This is done by transfering bytestream.
     * */
    private void copyDataBase() throws IOException{

        //Open your local db as the input stream
        InputStream myInput = this.context.getAssets().open(DATA_BASE_NAME);

        // Path to the just created empty db
        String outFileName = DATA_BASE_PATH + DATA_BASE_NAME;

        //Open the empty db as the output stream
        OutputStream myOutput = new FileOutputStream(outFileName);

        //transfer bytes from the inputfile to the outputfile
        byte[] buffer = new byte[1024];
        int length;
        while ((length = myInput.read(buffer))>0){
            myOutput.write(buffer, 0, length);
        }
        //Close the streams
        myOutput.flush();
        myOutput.close();
        myInput.close();

    }
}
