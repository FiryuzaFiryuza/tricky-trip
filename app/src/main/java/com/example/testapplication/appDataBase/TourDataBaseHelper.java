package com.example.testapplication.appDataBase;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.text.format.Time;

import com.example.testapplication.model.entities.CategoriesEntity;
import com.example.testapplication.model.entities.CurrentOptimumPlan;
import com.example.testapplication.model.entities.ReadyTourEntity;
import com.example.testapplication.model.entities.SightsEntity;
import com.example.testapplication.model.entities.SightsFromTourEntity;

import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by Фирюза on 16.11.2014.
 */
public class TourDataBaseHelper {

    private TourDataBase tourDataBase;
    private SQLiteDatabase database;

    public TourDataBaseHelper(Context context) throws IOException {
        this.tourDataBase = new TourDataBase(context);
        this.tourDataBase.createDataBase();
        this.open();
    }

    public void close() {
        this.tourDataBase.close();
        this.database.close();
    }

    public ArrayList<CategoriesEntity> getCategories() {
        ArrayList<CategoriesEntity> categoriesList = new ArrayList<CategoriesEntity>();

        String selectQuery = "SELECT * FROM " + TourDataBase.TABLE_CATEGORIES;

        Cursor cursor = this.database.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {
            do {
                CategoriesEntity categoryModel = new CategoriesEntity();
                categoryModel.setId(cursor.getInt(cursor.getColumnIndex(TourDataBase.KEY_ID)));
                categoryModel.setName(cursor.getString(cursor.getColumnIndex(TourDataBase.KEY_NAME)));
                categoriesList.add(categoryModel);
            } while (cursor.moveToNext());
        }
        cursor.close();
        return categoriesList;
    }

    public SightsEntity getSightsInfo(String name) {
        SightsEntity sightsInfoModel = new SightsEntity();
        sightsInfoModel.setName(name);

        String selectQuery = "SELECT * FROM " + TourDataBase.TABLE_SIGHTS + " WHERE " +
                TourDataBase.KEY_NAME + "=?";
        Cursor cursor = this.database.rawQuery(selectQuery, new String[] { name });

        int category_id = 0;
        CategoriesEntity categoriesModel = new CategoriesEntity();


        if(cursor != null) {
            cursor.moveToFirst();

            sightsInfoModel.setId(cursor.getInt(cursor.getColumnIndex(TourDataBase.KEY_ID)));

            sightsInfoModel.setAddress(cursor.getString(cursor.getColumnIndex(TourDataBase.KEY_ADDRESS)));

            //Time time = new Time(cursor.getString(cursor.getColumnIndex(TourDataBase.KEY_WORKING_TIME_FROM)));
            //sightsInfoModel.setFrom(time);

            //time = new Time(cursor.getString(cursor.getColumnIndex(TourDataBase.KEY_WORKING_TIME_TO)));
            //sightsInfoModel.setTo(time);

            //time = new Time(cursor.getString(cursor.getColumnIndex(TourDataBase.KEY_DINNER_TIME_FROM)));
            //sightsInfoModel.setDinnerTimeFrom(time);

            //time = new Time(cursor.getString(cursor.getColumnIndex(TourDataBase.KEY_DINNER_TIME_TO)));
            //sightsInfoModel.setDinnerTimeTo(time);

            //sightsInfoModel.setImage(cursor.getInt(cursor.getColumnIndex(TourDataBase.KEY_IMAGE)));

            sightsInfoModel.setDescription(cursor.getString(cursor.getColumnIndex(TourDataBase.KEY_DESCRIPTION)));

            category_id = cursor.getInt(cursor.getColumnIndex(TourDataBase.KEY_CATEGORIES_ID));
        }

        selectQuery = "SELECT * FROM " + TourDataBase.TABLE_CATEGORIES + " WHERE " +
                TourDataBase.KEY_ID + "=?";
        cursor = this.database.rawQuery(selectQuery, new String[] { String.valueOf(category_id) });

        categoriesModel.setId(category_id);

        if(cursor != null) {
            cursor.moveToFirst();

            categoriesModel.setName(cursor.getString(cursor.getColumnIndex(TourDataBase.KEY_NAME)));
        }

        cursor.close();

        sightsInfoModel.setCategory(categoriesModel);

        return sightsInfoModel;
    }

    public void addReadyTour(ReadyTourEntity entity) {
        ContentValues values = new ContentValues();
        entity.setSystemTour(false);
        values.put(TourDataBase.KEY_NAME, entity.getName());
        values.put(TourDataBase.KEY_IS_SYSTEM_TOUR, entity.isSystemTour());
        this.database.insert(TourDataBase.TABLE_READY_TOUR, null, values);
    }

    public void addCurrentOptimumPlan(CurrentOptimumPlan entity) {
        ContentValues values = new ContentValues();
        values.put(TourDataBase.KEY_SIGHTS_ID, entity.getSightsEntityId());
        values.put(TourDataBase.KEY_SEQUENCE_INDEX, entity.getSequenceIndex());
        this.database.insert(TourDataBase.TABLE_CURRENT_OPTIMUM_PLAN, null, values);
    }

    public void addSight(SightsFromTourEntity entity) {
        ContentValues values = new ContentValues();
        //values.put(TourDataBase.KEY_SIGHTS_ID, entity.getSightsEntityId());
        //values.put(TourDataBase.KEY_TOUR_ID, entity.getReadyTourId());
        // Inserting Row
        this.database.insert(TourDataBase.TABLE_CURRENT_OPTIMUM_PLAN, null, values);
    }

    public ReadyTourEntity getReadyTour(int id) {
        Cursor cursor = this.database.query(TourDataBase.TABLE_READY_TOUR, new String[] { TourDataBase.KEY_ID,
                        TourDataBase.KEY_NAME, TourDataBase.KEY_IS_SYSTEM_TOUR}, TourDataBase.KEY_ID + "=?",
                new String[] { String.valueOf(id) }, null, null, null, null);
        if (cursor != null)
            cursor.moveToFirst();

        ReadyTourEntity tour = new ReadyTourEntity(cursor.getInt(0),
                cursor.getString(1), cursor.getInt(2) == 1);

        return tour;
    }

    public int getReadyTourIdByName(String name) {
        int id = 0;
        String selectQuery = "SELECT * FROM " + TourDataBase.TABLE_READY_TOUR + " WHERE " +
                TourDataBase.KEY_NAME + "=?";
        Cursor cursor = this.database.rawQuery(selectQuery, new String[] { name });
        if (cursor.moveToFirst()) {
            id = cursor.getInt(0);
        }

        cursor.close();
        return id;
    }

    public ArrayList<ReadyTourEntity> getAllReadyTours() {
        ArrayList<ReadyTourEntity> tourList = new ArrayList<ReadyTourEntity>();
        String selectQuery = "SELECT  * FROM " + TourDataBase.TABLE_READY_TOUR ;
        //Cursor cursor = this.database.rawQuery(selectQuery, null);
        Cursor cursor = this.database.query(TourDataBase.TABLE_READY_TOUR,
                new String[] {TourDataBase.KEY_ID, TourDataBase.KEY_NAME, TourDataBase.KEY_IS_SYSTEM_TOUR},
                null, null,null,null,
                TourDataBase.KEY_NAME);

        if (cursor.moveToFirst()) {
            do {
                ReadyTourEntity tourModel = new ReadyTourEntity();
                tourModel.setId(cursor.getInt(0));
                tourModel.setName(cursor.getString(1));
                if(cursor.getInt(2) == 0) {
                    tourModel.setSystemTour(false);
                }
                else {
                    tourModel.setSystemTour(true);
                }

                tourList.add(tourModel);
            } while (cursor.moveToNext());
        }

        cursor.close();

        return tourList;
    }

    public ArrayList<SightsEntity> getAllSightsFromTour(int id) {
        ArrayList<SightsEntity> tourSightsList = new ArrayList<SightsEntity>();
        SightsEntity sightsModel;
        String selectQuery;
        Cursor cursor;

        /*
        String selectQuery = "SELECT "+ TourDataBase.TABLE_SIGHTS + "." + TourDataBase.KEY_NAME +
                ", " + TourDataBase.TABLE_SIGHTS + "." + TourDataBase.KEY_CATEGORIES_ID +
                " From " + TourDataBase.TABLE_SIGHTS_FROM_TOUR +
                " INNER JOIN " + TourDataBase.TABLE_READY_TOUR + " ON " +
                TourDataBase.TABLE_SIGHTS_FROM_TOUR + "." + TourDataBase.KEY_TOUR_ID + "=" +
                TourDataBase.TABLE_READY_TOUR + "." + TourDataBase.KEY_ID +
                " INNER JOIN " + TourDataBase.TABLE_SIGHTS + " ON " +
                TourDataBase.TABLE_SIGHTS_FROM_TOUR + "." + TourDataBase.KEY_SIGHTS_ID + "=" +
                TourDataBase.TABLE_SIGHTS + "." + TourDataBase.KEY_ID +
                " WHERE " + TourDataBase.KEY_TOUR_ID + "=" + id;
        */

        selectQuery = "SELECT " + TourDataBase.TABLE_SIGHTS_FROM_TOUR + "." + TourDataBase.KEY_SIGHTS_ID +
                " FROM " + TourDataBase.TABLE_SIGHTS_FROM_TOUR +
                " WHERE " + TourDataBase.KEY_TOUR_ID + "=?";
        cursor = this.database.rawQuery(selectQuery, new String[] { String.valueOf(id) });

        if (cursor.moveToFirst()) {
            do {

                String name = getSightNameById(cursor.getInt(cursor.getColumnIndex(TourDataBase.KEY_SIGHTS_ID)));

                sightsModel = getSightsInfo(name);

                tourSightsList.add(sightsModel);

            } while (cursor.moveToNext());
        }
        cursor.close();
        return tourSightsList;
    }

    public ArrayList<SightsEntity> getSightsFromCategoryById(int id) {
        ArrayList<SightsEntity> sightsList = new ArrayList<SightsEntity>();

        String selectQuery = "SELECT " + TourDataBase.KEY_NAME  + ", " + TourDataBase.KEY_CATEGORIES_ID +
                " FROM " + TourDataBase.TABLE_SIGHTS +
                " WHERE " + TourDataBase.KEY_CATEGORIES_ID + "=?";
        Cursor cursor = this.database.rawQuery(selectQuery, new String[] { String.valueOf(id) });
        if (cursor.moveToFirst()) {
            do {
                SightsEntity placeModel = new SightsEntity();
                placeModel.setName(cursor.getString(0));
                placeModel.setCategoriesId(cursor.getInt(1));
                sightsList.add(placeModel);
            } while (cursor.moveToNext());
        }
        cursor.close();

        return sightsList;
    }

    public int getSightIdByName(String name) {
        int id = 0;
        String selectQuery = "SELECT * FROM " + TourDataBase.TABLE_SIGHTS + " WHERE " +
                TourDataBase.KEY_NAME + "=?";
        Cursor cursor = this.database.rawQuery(selectQuery, new String[] { name });
        if (cursor.moveToFirst()) {
            id = cursor.getInt(0);
        }

        cursor.close();
        return id;
    }

    public String getSightNameById(int id) {
        String selectQuery = "SELECT " + TourDataBase.KEY_NAME +
                " FROM " + TourDataBase.TABLE_SIGHTS +
                " WHERE " + TourDataBase.KEY_ID + "=?";
        Cursor cursor = this.database.rawQuery(selectQuery, new String[] { String.valueOf(id) });

        cursor.moveToFirst();

        String name = cursor.getString(cursor.getColumnIndex(TourDataBase.KEY_NAME));

        return name;
    }

    public void editUserTour(String name, ArrayList<SightsEntity> deletedSights, ArrayList<SightsEntity> addedSights) {
        ArrayList<Integer> idSights = new ArrayList<Integer>();
        String selectQuery;

        long idTourName = getReadyTourIdByName(name);

        for (int i = 0; i < addedSights.size(); i++) {
            idSights.add(addedSights.get(i).getId());
        }

        ContentValues cv = new ContentValues();
        cv.put(TourDataBase.KEY_NAME, name);
        this.database.update(TourDataBase.TABLE_READY_TOUR, cv, TourDataBase.KEY_ID + "= ?", new String[] { String.valueOf(idTourName) });

        for (int i = 0; i < idSights.size(); i++) {
            selectQuery = "INSERT INTO " + TourDataBase.TABLE_SIGHTS_FROM_TOUR + " (" + TourDataBase.KEY_TOUR_ID +
                    ", " + TourDataBase.KEY_SIGHTS_ID + ") values(" + idTourName + ", " + idSights.get(i) + ");";
            this.database.execSQL(selectQuery);
        }

        idSights.clear();
        for (int i = 0; i < deletedSights.size(); i++) {
            idSights.add(deletedSights.get(i).getId());
        }

        for (int i = 0; i < idSights.size(); i++) {
            selectQuery = "DELETE FROM " + TourDataBase.TABLE_SIGHTS_FROM_TOUR +
                    " WHERE " + TourDataBase.KEY_SIGHTS_ID + "=" + idSights.get(i);
            this.database.execSQL(selectQuery);
        }
    }

    public void editSystemTour(String tourName, ArrayList<SightsEntity> sightsList, long id) {
        ArrayList<Integer> idSights = new ArrayList<Integer>();
        String selectQuery;
        long idTourName = id;

        for (int i = 0; i < sightsList.size(); i++) {
            idSights.add(sightsList.get(i).getId());
        }

        selectQuery = "INSERT INTO " + TourDataBase.TABLE_READY_TOUR + " (" +
                TourDataBase.KEY_NAME + ", " + TourDataBase.KEY_IS_SYSTEM_TOUR + ") values (" +
                tourName + ", " + 0 + ");";
        this.database.execSQL(selectQuery);

        for (int i = 0; i < sightsList.size(); i++) {
            selectQuery = "INSERT INTO " + TourDataBase.TABLE_SIGHTS_FROM_TOUR + " (" + TourDataBase.KEY_TOUR_ID +
                    ", " + TourDataBase.KEY_SIGHTS_ID + ") values(" + idTourName + ", " + idSights.get(i) + ");";
            this.database.execSQL(selectQuery);
        }
    }

    public void saveNewUserTour(String tourName, ArrayList<SightsEntity> deletedSights, ArrayList<SightsEntity> addedSights) {
        ArrayList<Integer> idSights = new ArrayList<Integer>();
        String selectQuery;
// TODO удалить из addedSights а потом вставлять в  таблицу

        selectQuery = "INSERT INTO " + TourDataBase.TABLE_READY_TOUR + " (" + TourDataBase.KEY_NAME +
                ") values(" + tourName  + ");";
        this.database.execSQL(selectQuery);

        int idTourName = getReadyTourIdByName(tourName);

        for (int i = 0; i < addedSights.size(); i++) {
            idSights.add(addedSights.get(i).getId());
        }

        for (int i = 0; i < idSights.size(); i++) {
            selectQuery = "INSERT INTO " + TourDataBase.TABLE_SIGHTS_FROM_TOUR + " (" + TourDataBase.KEY_TOUR_ID +
                    ", " + TourDataBase.KEY_SIGHTS_ID + ") values(" + idTourName + ", " + idSights.get(i) + ");";
            this.database.execSQL(selectQuery);
        }

        idSights.clear();
        for (int i = 0; i < deletedSights.size(); i++) {
            idSights.add(deletedSights.get(i).getId());
        }

        for (int i = 0; i < idSights.size(); i++) {
            selectQuery = "DELETE FROM " + TourDataBase.TABLE_SIGHTS_FROM_TOUR +
                    " WHERE " + TourDataBase.KEY_SIGHTS_ID + "=" + idSights.get(i);
            this.database.execSQL(selectQuery);
        }
    }

    private void open() throws SQLException {
        this.database = this.tourDataBase.getWritableDatabase();
    }
}
