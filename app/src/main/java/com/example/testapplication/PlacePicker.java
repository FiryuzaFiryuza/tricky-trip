package com.example.testapplication;


import android.app.Activity;
import android.app.AlertDialog;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import com.example.testapplication.appDataBase.TourDataBaseHelper;
import com.example.testapplication.fragment.PlacePickerFragment;
import com.example.testapplication.model.entities.ReadyTourEntity;
import com.example.testapplication.model.entities.SightsEntity;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;

import java.io.IOException;
import java.util.ArrayList;


    public class PlacePicker extends Activity  implements View.OnClickListener{

        private Button calculateButton;
        private Button saveBttn;
        private Button mapBttn;
        private Button listBttn;
        private Button chosenBttn;

        private PlacePickerFragment placePickerFragment;
        private GoogleMap googleMap;
        private MapFragment map;
        private FragmentManager fmanager;
        private FragmentTransaction ftransaction;

        public static boolean IsFromPreparedTour = false;
        public static String NewTourName;
        public static ArrayList<SightsEntity> NewDeletedItems = new ArrayList<SightsEntity>();
        public static ArrayList<SightsEntity> NewAddedItems = new ArrayList<SightsEntity>();

        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_place_picker);

            this.saveBttn = (Button)findViewById(R.id.bttn_save);
            this.mapBttn = (Button) findViewById(R.id.bttn_map);
            this.listBttn = (Button) findViewById(R.id.bttn_list);
            this.chosenBttn = (Button) findViewById(R.id.bttn_check_picked);
            this.calculateButton = (Button)findViewById(R.id.bttn_calculate);

            this.mapBttn.setOnClickListener(this);
            this.listBttn.setOnClickListener(this);
            this.saveBttn.setOnClickListener(this);
            this.chosenBttn.setOnClickListener(this);
            this.calculateButton.setOnClickListener(this);

            this.placePickerFragment = new PlacePickerFragment(TourData.TourName);

            this.fmanager = getFragmentManager();
            this.ftransaction = fmanager.beginTransaction();

            this.ftransaction.add(R.id.fl_sights_or_map_picker, placePickerFragment).commit();

            Bundle b = getIntent().getExtras();

            if (b != null) {
                if(b.containsKey("fromPreparedTour")) {
                    IsFromPreparedTour = b.getBoolean("fromPreparedTour");
                    //this.placePickerFragment.setTextFromEdit(TourData.TourName);
                }
                else {
                    IsFromPreparedTour = false;
                }
            }
        }


        @Override
        public boolean onCreateOptionsMenu(Menu menu) {
            getMenuInflater().inflate(R.menu.place_picker, menu);
            return true;
        }

        @Override
        public boolean onOptionsItemSelected(MenuItem item) {
            int id = item.getItemId();
            if (id == R.id.action_settings) {
                return true;
            }
            return super.onOptionsItemSelected(item);
        }

        @Override
        protected void onActivityResult(int requestCode, int resultCode, Intent data) {
            //Intent intent = getIntent();
            if (requestCode == 1) {
                if (resultCode == RESULT_OK) {
                    if (data.hasExtra("fromPreparedTour")) {
                        IsFromPreparedTour = data.getExtras().getBoolean("fromPreparedTour");
                        this.placePickerFragment.setTextFromEdit(TourData.TourName);
                    }
                    else {
                        IsFromPreparedTour = false;
                    }
                }
            }
        }

        public static void saveNewTour(View view, TourDataBaseHelper dataBaseHelper) {

            dataBaseHelper.saveNewUserTour(NewTourName, NewDeletedItems, NewAddedItems);

            NewDeletedItems.clear();
            NewAddedItems.clear();
        }

        @Override
        public void onClick(View view) {
            Intent intent;

            switch(view.getId()) {

                case R.id.bttn_map :
                    this.mapBttn.setVisibility(View.INVISIBLE);
                    this.listBttn.setVisibility(View.VISIBLE);
                    this.ftransaction = fmanager.beginTransaction();
                    this.map = new MapFragment();
                    this.ftransaction.replace(R.id.fl_sights_or_map_picker, this.map).commit();
                    this.googleMap = map.getMap();
                    break;

                case R.id.bttn_save :
                    TourDataBaseHelper dataBaseHelper = null;
                    try {
                        dataBaseHelper = new TourDataBaseHelper(view.getContext());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    if (IsFromPreparedTour) {
                        String tourName = this.placePickerFragment.getTextFromEdit();
                        if(TourData.ReadyTour.isSystemTour()) {
                            if(tourName == TourData.ReadyTour.getName())
                            {
                                openAlert(view, "Невозможно сохранить", "Тур с таким названием уже существует");
                            }
                            else {
                                ReadyTourEntity tour = new ReadyTourEntity();
                                tour.setName(tourName);
                                tour.setSystemTour(false);
                                TourData.ReadyTour = tour;

                                dataBaseHelper.addReadyTour(tour);
                                TourData.savePreparedTour(view, dataBaseHelper);
                            }
                        }
                        else {
                            if(tourName != TourData.ReadyTour.getName()) {
                                TourData.ReadyTour.setName(tourName);
                            }

                            TourData.savePreparedTour(view, dataBaseHelper);
                        }
                    }
                    else {
                        String tourName = placePickerFragment.getTextFromEdit();
                        if (tourName != null) {
                            ReadyTourEntity tour = new ReadyTourEntity();
                            tour.setName(tourName);
                            tour.setSystemTour(false);

                            dataBaseHelper.addReadyTour(tour);

                            saveNewTour(view, dataBaseHelper);
                        }
                        else {
                            this.openAlert(view, "Невозможно сохранить", "Введите название туру");
                        }
                    }

                    dataBaseHelper.close();

                    break;

                case R.id.bttn_list :

                    this.mapBttn.setVisibility(View.VISIBLE);
                    this.listBttn.setVisibility(View.INVISIBLE);
                    this.ftransaction  = fmanager.beginTransaction();
                    this.ftransaction.replace(R.id.fl_sights_or_map_picker, placePickerFragment).commit();

                    break;

                case R.id.bttn_calculate :

                    intent = new Intent(PlacePicker.this, CalculatePath.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    PlacePicker.this.startActivity(intent);

                    break;
                case R.id.bttn_check_picked :

                    intent = new Intent(PlacePicker.this, TourData.class);
                    intent.putExtra("fromChosenSights", this.placePickerFragment.getTextFromEdit());
                    startActivity(intent);

                    break;
            }
        }

        private void openAlert(View view, String title, String message) {
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(PlacePicker.this);
            alertDialogBuilder.setTitle(title);
            alertDialogBuilder.setMessage(message);

            alertDialogBuilder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    saveBttn.setVisibility(View.VISIBLE);
                    dialog.cancel();
                }
            });

            AlertDialog alertDialog = alertDialogBuilder.create();
            alertDialog.show();
        }
    }
