package com.example.testapplication;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import com.example.testapplication.appDataBase.TourDataBaseHelper;
import com.example.testapplication.fragment.ChosenDataTourList;
import com.example.testapplication.fragment.DataTourList;
import com.example.testapplication.model.entities.ReadyTourEntity;
import com.example.testapplication.model.entities.SightsEntity;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;

import java.io.IOException;
import java.util.ArrayList;


public class TourData extends Activity  implements View.OnClickListener {

    private Button addBttn;
    private Button saveBttn;
    private Button mapBttn;
    private Button listBttn;
    private static DataTourList dataTourList;
    private static ChosenDataTourList chosenDataTourList;
    private GoogleMap googleMap;
    private MapFragment map;
    private FragmentManager fmanager;
    private FragmentTransaction ftransaction;
    private static boolean fromPreparedTour = true;

    public static ReadyTourEntity ReadyTour = new ReadyTourEntity();
    public static String TourName;
    public static long TourId;
    public static ArrayList<SightsEntity> DeletedItems = new ArrayList<SightsEntity>();
    public static ArrayList<SightsEntity> AddedItems = new ArrayList<SightsEntity>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tour_data);
        setTitle("Список мест");

        this.saveBttn = (Button)findViewById(R.id.bttn_save);
        this.addBttn = (Button) findViewById(R.id.bttn_add_new);
        this.mapBttn = (Button) findViewById(R.id.bttn_map);
        this.listBttn = (Button) findViewById(R.id.bttn_list);
        this.mapBttn.setOnClickListener(this);
        this.listBttn.setOnClickListener(this);

        this.fmanager = getFragmentManager();
        this.ftransaction = fmanager.beginTransaction();
       // this.ftransaction.add(R.id.fl_sights_or_map, dataTourList).commit();

        Bundle name = getIntent().getExtras();
        this.filterFragments(name);


        this.addBttn.setOnClickListener(this);
        this.saveBttn.setOnClickListener(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.tour_data, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void filterFragments(Bundle name) {
        if(name.containsKey("item")) {
            dataTourList = new DataTourList();
            dataTourList.setName(name.getString("item"));

            this.ftransaction.add(R.id.fl_sights_or_map, dataTourList).commit();
        }
        else if (name.containsKey("fromChosenSights")) {
            chosenDataTourList = new ChosenDataTourList();
            chosenDataTourList.setName(name.getString("fromChosenSights"));

            fromPreparedTour = PlacePicker.IsFromPreparedTour;

            this.ftransaction.add(R.id.fl_sights_or_map, chosenDataTourList).commit();
        }
    }

    private void openAlert(View view, String title, String message) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(TourData.this);
        alertDialogBuilder.setTitle(title);
        alertDialogBuilder.setMessage(message);

        alertDialogBuilder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
            }
        });

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    @Override
    public void onClick(View view) {
        switch(view.getId()) {

            case R.id.bttn_add_new :
                //openAlert(view);
                TourName = dataTourList.getTextFromEdit();
                Intent intent = new Intent(TourData.this, PlacePicker.class);
                intent.putExtra("fromPreparedTour", fromPreparedTour);
                startActivity(intent);
                break;

            case R.id.bttn_map :

                this.mapBttn.setVisibility(View.INVISIBLE);
                this.listBttn.setVisibility(View.VISIBLE);
                this.ftransaction = fmanager.beginTransaction();
                this.map = new MapFragment();
                this.ftransaction.replace(R.id.fl_sights_or_map, this.map).commit();
                this.googleMap = map.getMap();
                break;

            case R.id.bttn_save :

                TourDataBaseHelper dataBaseHelper = null;
                try {
                    dataBaseHelper = new TourDataBaseHelper(view.getContext());
                } catch (IOException e) {
                    e.printStackTrace();
                }

                if(fromPreparedTour) {
                    String name = DataTourList.getTextFromEdit();
                    Log.d("activity", name);
                    if(ReadyTour.isSystemTour()) {
                        if(ReadyTour.getName().equals(name) && name != null) {
                            this.openAlert(view, "Невозможно сохранить", "Тур с таким названием уже существует");
                        }
                        else {
                            ReadyTourEntity tour = new ReadyTourEntity();
                            tour.setName(name);
                            tour.setSystemTour(false);

                            dataBaseHelper.addReadyTour(tour);
                            savePreparedTour(view, dataBaseHelper);
                        }
                    }
                    else {
                        if(dataTourList.getTextFromEdit() == null) {
                            this.openAlert(view, "Невозможно сохранить", "Введите название туру");
                        }
                        else {
                            ReadyTourEntity tour = new ReadyTourEntity();
                            tour.setName(name);
                            tour.setSystemTour(false);

                            dataBaseHelper.addReadyTour(tour);

                            savePreparedTour(view, dataBaseHelper);
                        }
                    }
                }
                else {
                    String tourName = chosenDataTourList.getTextFromEdit();
                    if (tourName != null) {
                        ReadyTourEntity tour = new ReadyTourEntity();
                        tour.setName(tourName);
                        tour.setSystemTour(false);

                        dataBaseHelper.addReadyTour(tour);

                        savePreparedTour(view, dataBaseHelper);
                    }
                    else {
                        this.openAlert(view, "Невозможно сохранить", "Введите название туру");
                    }
                }

                dataBaseHelper.close();

                break;

            case R.id.bttn_list :
                this.mapBttn.setVisibility(View.VISIBLE);
                this.listBttn.setVisibility(View.INVISIBLE);
                this.ftransaction  = fmanager.beginTransaction();
                if(fromPreparedTour) {
                    this.ftransaction.replace(R.id.fl_sights_or_map, dataTourList).commit();
                }
                else {
                    this.ftransaction.replace(R.id.fl_sights_or_map, chosenDataTourList).commit();
                }
                break;
        }
    }

    public static void savePreparedTour(View view, TourDataBaseHelper dataBaseHelper) {

        if(fromPreparedTour) {
           // if (ReadyTour.isSystemTour()) {
           //     dataBaseHelper.editSystemTour(ReadyTour.getName(), dataTourList.getAdapter().getSightsList(), TourId);
           // }
           // else {
                dataBaseHelper.editUserTour(ReadyTour.getName(), DeletedItems, AddedItems);
            //}

            DeletedItems.clear();
            AddedItems.clear();
        }
        else {
            PlacePicker.saveNewTour(view, dataBaseHelper);
        }
    }

  /*  @Override
    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
        this.ftransaction = fmanager.beginTransaction();
       // map = ((MapFragment)getFragmentManager().findFragmentById(R.id.f_mapView));
        this.map = new MapFragment();
        if(b) {
            this.ftransaction.replace(R.id.fl_sights_or_map, this.map);
            this.googleMap = map.getMap();
            *//*this.googleMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {

                @Override
                public void onMapClick(LatLng point) {
                    MarkerOptions market = new MarkerOptions().position(
                            new LatLng(point.latitude, point.longitude)).title("New Marker");
                    googleMap.addMarker(market);
                }
            });*//*
        }
        else {
            this.ftransaction.replace(R.id.fl_sights_or_map, this.dataTourList);
        }
        this.ftransaction.commit();
    }*/
}
